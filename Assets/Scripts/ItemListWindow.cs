using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ItemListWindow : MonoBehaviour
{
    [SerializeField] Sprite titleBackground;
    [SerializeField] string titleText;

    [SerializeField] List<string> items;
    [SerializeField] GameObject listItemPrefab;

    Transform itemListContent;

    // Start is called before the first frame update
    void Start()
    {
        itemListContent = transform.Find("Content/Item List/Viewport/Content");
        UpdateTitle();
        PopulateContentList();
    }

    void UpdateTitle()
	{
        Transform title = transform.Find("Heading/Title");
        Image titleBackgroundImage = title.transform.Find("Background").GetComponent<Image>();
        titleBackgroundImage.sprite = titleBackground;
        TMP_Text titleTMP = title.transform.Find("Background/Text").GetComponent<TMP_Text>();
        titleTMP.text = titleText;
    }

    void PopulateContentList()
    {
        foreach (var item in items)
		{
            GameObject gameObject = Instantiate(listItemPrefab, itemListContent);
            TMP_Text listItemTMP = gameObject.transform.Find("Text").GetComponent<TMP_Text>();
            listItemTMP.text = item;
        }
	}

    // Update is called once per frame
    void Update()
    {
        
    }
}
