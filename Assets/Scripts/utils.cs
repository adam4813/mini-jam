﻿class utils
{
	public static bool InRange(int value, int min, int max) => value >= min && value < max;
}