using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GemList : MonoBehaviour
{
    [SerializeField] GameObject gemPrefab;
    [SerializeField] List<Sprite> gemSprites;
    [SerializeField] int gridColumns;
    [SerializeField] int gridRows;
    [SerializeField] Transform gridContainer;

    GameObject selectGem;
    GameObject[][] gemSlots;

    // Start is called before the first frame update
    void Start()
    {
        gemSlots = new GameObject[gridRows][];
        for (int r = 0; r < gridRows; r++)
        {
            gemSlots[r] = new GameObject[gridColumns];
            for (int c = 0; c < gridColumns; c++)
            {
                GameObject gem = Instantiate(gemPrefab, gridContainer);
                gemSlots[r][c] = gem;
                gem.name = $"{r}:{c}";

                Image gemImage = gem.GetComponent<Image>();
                int randomSpriteIndex = Random.Range(0, gemSprites.Count);
                gemImage.sprite = gemSprites[randomSpriteIndex];

                gem.GetComponent<Button>().onClick.AddListener(() =>
                {
                    if (selectGem == null)
                    {
                        selectGem = gem;
                        return;
                    }
                    if (selectGem != gem && CanSwap(selectGem, gem))
                    {
                        SwapGem(selectGem, gem);
                        CheckMatches();
                    }
                    selectGem = null;
                });
            }
        }
    }

    bool CanSwap(GameObject gemA, GameObject gemB)
    {
        int[] slotCoords = NameToCoord(gemA.name);

        return GemInSlot(gemB, slotCoords[0] - 1, slotCoords[1]) ||
            GemInSlot(gemB, slotCoords[0] + 1, slotCoords[1]) ||
            GemInSlot(gemB, slotCoords[0], slotCoords[1] - 1) ||
            GemInSlot(gemB, slotCoords[0], slotCoords[1] + 1);
    }

    bool GemInSlot(GameObject gem, int row, int column) => CoordsInBounds(row, column) && gemSlots[row][column] == gem;

    bool CoordsInBounds(int row, int column) => utils.InRange(row, 0, gridRows) && utils.InRange(column, 0, gridColumns);

    void SwapGem(GameObject gemA, GameObject gemB)
    {
        Debug.Log($"Swapping gems {gemA.name} <-> {gemB.name}");

        int siblingIndex = gemA.transform.GetSiblingIndex();
        gemA.transform.SetSiblingIndex(gemB.transform.GetSiblingIndex());
        gemB.transform.SetSiblingIndex(siblingIndex);

        int[] coordA = NameToCoord(gemA.name);
        int[] coordB = NameToCoord(gemB.name);
        gemSlots[coordA[0]][coordA[1]] = gemB;
        gemSlots[coordB[0]][coordB[1]] = gemA;

        string name = gemA.name;
        gemA.name = gemB.name;
        gemB.name = name;
    }

    int[] NameToCoord(string name)
    {
        string[] coord = name.Split(":");
        int[] coords = { int.Parse(coord[0]), int.Parse(coord[1]) };
        return coords;
    }

    public void CheckMatches()
	{

	}
}
